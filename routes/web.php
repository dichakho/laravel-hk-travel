<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('admin', 'adminController@getAdminView');
Route::get('admin/dashboard', 'adminController@getAdminViewDashboard');
Route::group(['prefix' => 'admin'], function(){
  Route::group(['prefix' => 'post'], function(){
    Route::get('list', 'postController@getList');
    Route::get('add', 'postController@getAdd');
    Route::get('edit', 'postController@getEdit');
  });
  Route::group(['prefix' => 'category'], function(){
    Route::get('list', 'categoryController@getList');
    Route::get('add', 'categoryController@getAdd');
    Route::get('edit', 'categoryController@getEdit');
  });
});