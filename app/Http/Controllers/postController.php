<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class postController extends Controller
{
    //
    function getList(){
        return view('admin.post.list');
    }
    function getAdd(){
        return view('admin.post.add');
    }
    function getEdit(){
        return view('admin.post.edit');
    }
}
