<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class categoryController extends Controller
{
    //
    function getList(){
        return view('admin.category.list');
    }
    function getAdd(){
        return view('admin.category.add');
    }
    function getEdit(){
        return view('admin.category.edit');
    }
}
