<!-- Jquery JS-->
<script src="admin_resource/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="admin_resource/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="admin_resource/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="admin_resource/vendor/slick/slick.min.js">
    </script>
    <script src="admin_resource/vendor/wow/wow.min.js"></script>
    <script src="admin_resource/vendor/animsition/animsition.min.js"></script>
    <script src="admin_resource/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="admin_resource/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="admin_resource/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="admin_resource/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="admin_resource/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="admin_resource/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="admin_resource/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="admin_resource/js/main.js"></script>
