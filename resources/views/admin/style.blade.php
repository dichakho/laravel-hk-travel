<!-- Fontfaces CSS-->
<base href="{{asset('')}}">
<link href="admin_resource/css/font-face.css" rel="stylesheet" media="all">
    <link href="admin_resource/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="admin_resource/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="admin_resource/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="admin_resource/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="admin_resource/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="admin_resource/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="admin_resource/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="admin_resource/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="admin_resource/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="admin_resource/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="admin_resource/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="admin_resource/css/theme.css" rel="stylesheet" media="all">