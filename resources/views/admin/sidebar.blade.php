<aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="admin">
                    <img src="admin_resource/images/icon/logo.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a class="js-arrow" href="admin/dashboard">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                            <i class="fas fa-newspaper"></i>Post</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="admin/post/list">List</a>
                                </li>
                                <li>
                                    <a href="admin/post/add">Add</a>
                                </li>
                                <li>
                                    <a href="admin/post/edit">Edit</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-table"></i>Category</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="admin/category/list">List</a>
                                </li>
                                <li>
                                    <a href="admin/category/add">Add</a>
                                </li>
                                <li>
                                    <a href="admin/category/edit">Edit</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-table"></i>User</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="admin/category/list">List</a>
                                </li>
                                <li>
                                    <a href="admin/category/add">Add</a>
                                </li>
                                <li>
                                    <a href="admin/category/edit">Edit</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>